<?php

require_once 'vendor/autoload.php';

$url = __DIR__.'/movies.cypher';

$query = trim(file_get_contents($url));
$connection->sendCypherQuery($query);
?>